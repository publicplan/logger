import {LoggingEngine} from './LoggingEngine';
import {LogLevel, LogMessage} from './Types';
import winstonLoggingEngine from './winstonLoggingEngine';

export class Logger implements LoggingEngine {
  private readonly logger: LoggingEngine;

  constructor(logger: LoggingEngine = winstonLoggingEngine) {
    this.logger = logger;
  }

  public log(level: LogLevel | LogMessage, message?: string, metadata?: object) {
    if (level instanceof LogMessage) {
      this.logger[level.level](level.message, level.metadata);
    } else {
      this.logger[level](message, metadata);
    }
  }

  public error(message: string, metadata?: object) {
    this.logger.error(message, metadata);
  }

  public warn(message: string, metadata?: object) {
    this.logger.warn(message, metadata);
  }

  public info(message: string, metadata?: object) {
    this.logger.info(message, metadata);
  }

  public verbose(message: string, metadata?: object) {
    this.logger.verbose(message, metadata);
  }

  public debug(message: string, metadata?: object) {
    this.logger.debug(message, metadata);
  }

  public silly(message: string, metadata?: object) {
    this.logger.silly(message, metadata);
  }
}
