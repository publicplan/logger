import {Logger} from './Logger';

const logger = new Logger();

export default logger;

export {LoggingEngine} from './LoggingEngine';

export {LogLevel, LogMessage} from './Types';

export {Logger as Logger};
