import {createLogger, format, transports} from 'winston';
import {LoggingEngine} from './LoggingEngine';

const {combine, timestamp, simple, prettyPrint} = format;
let formatters = combine(timestamp(), prettyPrint());
if (process.env.NODE_ENV !== 'production') {
  formatters = combine(simple());
}
const winstonLoggingEngine = createLogger({
  level: process.env.LOG_LEVEL,
  format: formatters,
  transports: [new transports.Console()],
}) as LoggingEngine;

export default winstonLoggingEngine;
