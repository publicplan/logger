export class LogMessage {
  public level: LogLevel;
  public message: string;
  public metadata?: object;

  constructor(level: LogLevel, message: string, metadata?: object) {
    this.level = level;
    this.message = message;
    this.metadata = metadata;
  }
}

export enum LogLevel {
  error = 'error',
  warn = 'warn',
  info = 'info',
  verbose = 'verbose',
  debug = 'debug',
  silly = 'silly',
}
