import {LogLevel, LogMessage} from './Types';

export interface LoggingEngine {
  log(level: LogLevel | LogMessage, message?: string, metadata?: object);

  error(message: string, metadata?: object);

  warn(message: string, metadata?: object);

  info(message: string, metadata?: object);

  verbose(message: string, metadata?: object);

  debug(message: string, metadata?: object);

  silly(message: string, metadata?: object);
}
