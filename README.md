
# Logger

Erweitarbare Logger Instanz mit vernünftigen Standardkonfigurationen.

## Quick Start
Es wird eine vorkonfigurierte Logger-Instanz bereitgestellt.
```typescript
  import logger from 'logger';

  logger.info('Hier ist eine Info');
```  

## Abweichende Winston Logger Konfiguration
Eine neue Instanz der Logger Klasse kann mit abweichenden Konfigurationen wie folgt erstellt werden.

```typescript
  import {Logger} from 'logger';
  import {createLogger, format, transports} from 'winston';
  
  const {combine, timestamp, prettyPrint} = format;
  const winstonLoggingEngine = createLogger({
      level: 'error',
      format: combine(timestamp(), prettyPrint()),
      transports: [new transports.Console()],
  });
  
  const logger = new Logger(winstonLoggingEngine);
  
  logger.info('Hier ist eine Info');
```

Weitere Konfigurationsmöglichkeiten kann befinden sich [hier](https://github.com/winstonjs/winston).

## Eigener Logger-Engine
Ein eigener Logger *Engine* kann für den Logger verwendet werden. Diese *Engine* muss die `LoggingEngine` Interface 
implementieren. 
```typescript
  import {Logger, LoggingEngine} from 'logger';
  
  class WowLoggerEngine implements LoggingEngine{
    //...
  }
  const logger = new Logger(new WowLoggerEngine());
  
  logger.info('Hier ist eine Info');
```
 
## Installation

```bash
 npm install git+https://git@bitbucket.org/publicplan/logger.git
```

## Tests ausführen
```bash
npm test
```
