import {LogLevel, LogMessage} from '../src';
import {Logger} from '../src/Logger';

let logger: Logger;
let fakeLoggingEngine;
beforeEach(() => {
  fakeLoggingEngine = {
    error: jest.fn(),
    warn: jest.fn(),
    info: jest.fn(),
    verbose: jest.fn(),
    debug: jest.fn(),
    silly: jest.fn(),
  };
  logger = new Logger(fakeLoggingEngine);
});

describe('Winston loggerInstance', () => {
  it('logs a message object', () => {
    const metadata = {meta: 'data'};
    const info = 'Info';
    const message = new LogMessage(LogLevel.info, info, metadata);

    logger.log(message);

    expect(fakeLoggingEngine.info).toHaveBeenCalledWith(info, metadata);
  });

  it('logs a call with the level, message and metadata', () => {
    logger.log(LogLevel.error, 'Error', {});

    expect(fakeLoggingEngine.error).toHaveBeenCalledWith('Error', {});
  });

  it('logs errors', () => {
    logger.error('Error', {});

    expect(fakeLoggingEngine.error).toHaveBeenCalledWith('Error', {});
  });

  it('logs warnings', () => {
    logger.warn('Warning', {});

    expect(fakeLoggingEngine.warn).toHaveBeenCalledWith('Warning', {});
  });

  it('logs infos', () => {
    logger.info('Info', {});

    expect(fakeLoggingEngine.info).toHaveBeenCalledWith('Info', {});
  });

  it('logs verbose logs', () => {
    logger.verbose('Verbose', {});

    expect(fakeLoggingEngine.verbose).toHaveBeenCalledWith('Verbose', {});
  });

  it('logs debug logs', () => {
    logger.debug('Debug', {});

    expect(fakeLoggingEngine.debug).toHaveBeenCalledWith('Debug', {});
  });

  it('logs silly logs', () => {
    logger.silly('Are you kidding?', {});

    expect(fakeLoggingEngine.silly).toHaveBeenCalledWith('Are you kidding?', {});
  });
});
